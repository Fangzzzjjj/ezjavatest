package JavaPracticeFromCSDN.Practice1_2016_7_12;


import java.util.Scanner;

/**
 * Created by fangzzzjjj on 16/7/12.
 */
public class Practice1 {
    public static void main(String[] args) {
        System.out.println("----水仙花数----");
        for (int i =100;i<1000;i++){
            int ge=i%10;
            int shi=(i/10)%10;
            int bai=(i/100)%10;
            if(ge*ge*ge+shi*shi*shi+bai*bai*bai==i){
                System.out.println(i);
            }
        }
        System.out.println("----正整数分解----");

        Scanner sc = new Scanner(System.in);
        int i = sc.nextInt();
        System.out.println(i);
        int[] prime=new int[(int) Math.sqrt(i)];
        prime[0]=2;
        int temp=3;
        int index=1;
        while (temp<= Math.sqrt(i)){
            if (isPrime(temp)){
                prime[index]=temp;
                index++;
            }
            temp=temp+2;
        }
//        for (int j=0;j<index;j++){
//            System.out.println(prime[j]);
//        }
        int tempIndex=0;
        while (true){
            if(i%prime[tempIndex]==0){
                i=i/prime[tempIndex];
                System.out.print(prime[tempIndex]+"*");
            }else{
                tempIndex++;
            }
            if(prime[tempIndex]==0){
                break;
            }
        }
    }

    public static boolean isPrime(int n) {
        if(n==2){
            return true;
        }
        if (n % 2 == 0) {
            return false;
        }
        for (int num = 3; num < Math.sqrt(n); num += 2) {
            if (n % num == 0) return false;
        }
        return true;
    }
}
//Q1耗时2min
//Q2耗时14min