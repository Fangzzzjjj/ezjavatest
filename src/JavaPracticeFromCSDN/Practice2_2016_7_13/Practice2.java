package JavaPracticeFromCSDN.Practice2_2016_7_13;

import java.util.Scanner;

/**
 * Created by fangzzzjjj on 16/7/13.
 */
public class Practice2 {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        int grade = sc.nextInt();
        assert grade>=0;
        if(grade>=90){
            System.out.println("A");
        }
        else if(grade>=60){
            System.out.println("B");
        }else{
            System.out.println("C");
        }
        System.out.println("-----------");
        int num1=sc.nextInt();
        int num2=sc.nextInt();
        if(num1<num2){
            int temp =num1;
            num1=num2;
            num2=num1;
        }
        int m=gcd(num1,num2);
        System.out.println("最大公约数"+m);
        System.out.println("最小公倍数"+num2*num1/m);
        System.out.println("-----------");
        sc.nextLine(); //flush
        String str=sc.nextLine();
        char[] chars =str.toCharArray();
        int eng=0;
        int space=0;
        int num=0;
        int other=0;
        for(int i =0;i< chars.length;i++){
            if(Character.isLetter(chars[i])){
                eng++;
            }else if(Character.isSpaceChar(chars[i])){
                space++;
            }else if(Character.isDigit(chars[i])){
                num++;
            }else {
                other++;
            }
        }
        System.out.println(eng+";"+space+";"+num+";"+other);
    }

    private static int gcd(int num1, int num2) {
        int i =num1%num2;
        if(i==0){
            return num2;
        }else {
            return gcd(num2,i);
        }
    }
}
//题目一 1min(全在打字上...)
//题目二 7min
//题目三 13min 被Scanner.nextInt坑了,google了之后明白。
// 用完nextInt流结尾没有换行,再用nextLine不行,需要先flush下