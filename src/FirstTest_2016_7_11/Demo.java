package FirstTest_2016_7_11;

/**
 * Created by fangzzzjjj on 16/7/10.
 */
public class Demo {
    public static void main(String[] args) throws OutOfBoundary {
        int[] fibTestCase = {2, 10, 15};
        for (int i : fibTestCase) {
            fib(i);
        }
//        fib(-1); 抛越界异常

        System.out.println("----------------------------");
        int[] isPrimeTestCase = {2, 3, 17, 597,587};
        for (int i : isPrimeTestCase) {
            System.out.println(isPrime(i));
        }
        System.out.println("----------------------------");


        MyChain<Integer> test = new MyChain();
        test.add(1);
        test.add(2);
        test.add(3);
        test.add(4);
        test.show();
        System.out.println("------");
//        test.delete(-1); 抛越界异常
//        test.delete(4); 抛越界异常
        test.delete(3);
        test.show();
        System.out.println("------");
        test.change(1, 99);
        test.show();
        System.out.println("------");
        test.change(0, 99);
        test.show();
//        test.change(-1,99); 抛越界异常
        System.out.println("----------------------------");
        BinaryTree bt = new BinaryTree();
        bt.add(5);
        bt.add(4);
        bt.add(6);
        bt.add(99);
        bt.add(78);
        bt.add(55);
        bt.add(455);
        bt.show();
        System.out.println("\n----------------------------");
        bt.swap();
        bt.show();
    }


    static void fib(int n) throws OutOfBoundary {
        if (n < 2) {
            throw new OutOfBoundary();
        }
        int[] fib = new int[n];
        fib[0] = 1;
        fib[1] = 1;
        for (int i = 2; i < n; i++) {
            fib[i] = fib[i - 1] + fib[i - 2];
        }
        System.out.print("[");
        for (int i = 0; i < n - 1; i++) {
            System.out.print(fib[i] + ",");
        }
        System.out.print(fib[n - 1] + "]" + "\n");
    }

    public static boolean isPrime(int n) throws OutOfBoundary{
        assert n>0:"请输入一个自然数";
        if (n % 2 == 0) return false;
        for (int num = 3; num < Math.sqrt(n); num += 2) {
            if (n % num == 0) return false;
        }
        return true;
    }
}
