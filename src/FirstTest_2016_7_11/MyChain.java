package FirstTest_2016_7_11;

/**
 * Created by fangzzzjjj on 16/7/10.
 */
class MyChain<Item> {
    private Node head; //头指针
    private Node last;
    public int size;

    MyChain() {
        this.head = this.last = null;
        this.size = 0;
    }

    private class Node {
        Item item;
        Node next;

        Node(Item item) {
            this.item = item;
            next = null;
        }
    }

    /**
     * 增
     *
     * @param item
     */
    void add(Item item) {
        Node node = new Node(item);
        if (head == null) {
            head = node;
        } else {
            last.next = node;
        }
        last = node;
        size++;
    }

    /**
     * 删除索引处元素
     *
     * @param index
     */
    public void delete(int index) throws OutOfBoundary {
        if (index > size - 1 || index < 0) {
            throw new OutOfBoundary();
        }
        if (index == 0) {
            head = head.next;
            return;
        }
        Node node = head;
        int i = 0;
        while (i < index - 1) {
            node = node.next;
            i++;
        }
        node.next = node.next.next;
    }

    public void change(int index, Item item) throws OutOfBoundary {
        if (index > size - 1 || index < 0) {
            throw new OutOfBoundary();
        }
        Node node = head;
        int i = 0;
        while (i < index) {
            node = node.next;
            i++;
        }
        node.item = item;
    }

    /**
     * 查
     */
    public void show() {
        show(head);
    }

    private void show(Node head) {
        if (head != null) {
            System.out.println(head.item);
            Node node = head.next;
            show(node);
        }
    }

//    public int length(){
//        return size;
//    }

}

