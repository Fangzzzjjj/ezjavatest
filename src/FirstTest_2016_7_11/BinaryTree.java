package FirstTest_2016_7_11;

/**
 * Created by fangzzzjjj on 16/7/11.
 */

public class BinaryTree {
    private Node root;

    public BinaryTree() {
        root = null;
    }

    private class Node {
        int num;
        Node left;
        Node right;

        Node(int num) {
            this.num = num;
            left = right = null;
        }
    }

    public void add(int num) {
        root = add(root, num);
    }

    public Node add(Node node, int num) {
        if (node == null) {
            node = new Node(num);
        } else {
            if (node.num > num) {       //小的放左边
                node.left = add(node.left, num);
            } else {
                node.right = add(node.right, num);
            }
        }
        return (node);
    }

    public void show() {
        show(root);
    }

    private void show(Node root) {      //前序遍历 中 左 右;
        if (root != null) {
            System.out.print(root.num + "-");
            show(root.left);
            show(root.right);
        }else {
            System.out.print("#-");
        }
    }

    public void swap() {
        swap(root);
    }

    private void swap(Node root) {
        if (root == null) return;
        if ((root.left == null) && (root.right == null)) return;
        Node temp = root.left;
        root.left = root.right;
        root.right = temp;
        swap(root.left);
        swap(root.right);
    }

}
