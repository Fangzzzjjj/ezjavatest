package DataStuctrue.IdoitLinkedListByMyself;


import java.util.ArrayList;

/**
 * Created by fangzzzjjj on 16/7/13.
 */
public class MyChain<Item> {
    private Node head; //头指针
    private Node last;
    private int size;

    MyChain() {
        this.head = this.last = null;
        this.size = 0;
    }

    /**
     * Construct by an array
     *
     * @param items
     */
    MyChain(Item[] items) {
        for (int i = 0; i < items.length; i++) {
            add(items[i]);
        }
    }

    /**
     * Initialize a chain with item ,repeat num times
     *
     * @param item
     * @param num
     */
    MyChain(Item item, int num) {
        for (int i = 0; i < num; i++) {
            add(item);
        }
    }

    /**
     * Construct by a arraylist
     *
     * @param arrayList
     */
    MyChain(ArrayList<Item> arrayList) {
        for (Item item : arrayList) {
            add(item);
        }
    }


    private class Node {
        Item item;
        Node next;

        Node(Item item) {
            this.item = item;
            next = null;
        }
    }

    /**
     * add an item at the end of the chain
     *
     * @param item
     */
    void add(Item item) {
        Node node = new Node(item);
        if (head == null) {
            head = node;
        } else {
            last.next = node;
        }
        last = node;
        size++;
    }

    /**
     * add an item at index
     *
     * @param index
     * @param item
     * @throws OutOfBoundary
     */
    void add(int index, Item item) throws OutOfBoundary {
        if (index > size - 1 || index < 0) {
            throw new OutOfBoundary();
        }
        Node node = new Node(item);
        if (index == 0) {
            node.next = head;
            head = node;
            size++;
            return;
        }
        int i = 0;
        Node indexNode = head;
        while (i < index - 1) {
            indexNode = indexNode.next;
            i++;
        }
        node.next = indexNode.next;
        indexNode.next = node;
        size++;
        return;
    }

    public void add(MyChain<Item> chain) {
        last.next = chain.head;
    }


    /**
     * delete the item at index
     *
     * @param index
     */
    public void delete(int index) throws OutOfBoundary {
        if (index > size - 1 || index < 0) {
            throw new OutOfBoundary();
        }
        if (index == 0) {
            head = head.next;
            size--;
            return;
        }
        Node node = head;
        int i = 0;
        while (i < index - 1) {
            node = node.next;
            i++;
        }
        node.next = node.next.next;
        if(index==size-1){
            last=node;
        }
        size--;
    }

    /**
     * change the item at index
     *
     * @param index
     * @param item
     * @throws OutOfBoundary
     */
    public void change(int index, Item item) throws OutOfBoundary {
        if (index > size - 1 || index < 0) {
            throw new OutOfBoundary();
        }
        Node node = head;
        int i = 0;
        while (i < index) {
            node = node.next;
            i++;
        }
        node.item = item;
    }

    /**
     * show the whole chain
     */
    public void show() {
        show(head);
    }

    /**
     * show the item at index
     *
     * @param index
     * @throws OutOfBoundary
     */
    public void show(int index) throws OutOfBoundary {
        if (index > size - 1 || index < 0) {
            throw new OutOfBoundary();
        }
        Node node = head;
        int i = 0;
        while (i < index - 1) {
            node = node.next;
            i++;
        }
        System.out.println(node.next.item);
    }

    private void show(Node head) {
        if (head != null) {
            System.out.println(head.item);
            Node node = head.next;
            show(node);
        }
    }

    /**
     * show the length of the chain
     *
     * @return
     */
    public int length() {
        return size;
    }

    /**
     * reverse the chain
     */
    public void reverse() {
        if (size == 1) {
            return;
        }
        for (int i = size - 1; i > 0; i--) {
            getNode(i).next = getNode(i - 1);
        }
        head.next = null;
        Node temp = head;
        head = last;
        last = temp;
    }

    /**
     * @return chain
     */
    public MyChain<Item> clone(){
        MyChain<Item> chain = new MyChain<>();
        Node head =this.head;
        for (int i=0;i<this.size;i++){
            chain.add(head.item);
            head=head.next;
        }
        return chain;
    }

    /**
     * @param item
     * @return index
     */
    public int index(Item item){
        Node head=this.head;
        for(int i=0;i<this.size;i++){
            if (head.item.equals(item)){
                return i;
            }
            head=head.next;
        }
        return -1;
    }

    /**
     *
     * @param item
     * @return boolean
     */
    public boolean contain(Item item){
        return (index(item)==-1)?false:true;
    }


    private Node getNode(int index) {
        Node node = head;
        int i = 0;
        while (i < index) {
            node = node.next;
            i++;
        }
        return node;
    }

    /**
     *
     */
    public void clear(){
        head=null;
        last=null;
        size=0;
    }
}

