package DataStuctrue.IdoitLinkedListByMyself;

import java.util.ArrayList;

/**
 * Created by fangzzzjjj on 16/7/13.
 */
public class Demo {
    public static void main(String[] args) throws OutOfBoundary {
        MyChain<Integer> test = new MyChain();
        test.add(1);
        test.add(2);
        test.add(3);
        test.add(4);
//        test.show();
//        System.out.println("------");
//        test.add(0,99);
//        test.show();
//        System.out.println("------");
//        test.add(4,99);
//        test.show();
//        System.out.println("现有元素"+test.length()+"个");
//        System.out.println("------");
////        test.add(6,100); 抛异常
////        test.delete(-1); 抛越界异常
////        test.delete(4); 抛越界异常
//        test.delete(3);
//        test.show();
//        System.out.println("------");
//        test.change(1, 999);
//        test.show();
//        System.out.println("------");
//        test.change(0, 999);
//        test.show();
//        System.out.println("------");
//        System.out.print("索引4对应的元素是");
//        test.show(4);
//        System.out.println("------");
//        System.out.println("测试翻转");
//        for(int i=0;i<test.length();i++){
//            test.change(i,i);
//        }
//        test.show();
//        System.out.println("-------------");
//        test.reverse();
//        test.show();

//        MyChain<String> test2 = new MyChain();  //Compile Error
//        test2.add("a");
//        test2.add("b");
//        test2.add("c");
//        test.add(test2);
        MyChain<Integer> test2 = new MyChain();
        test2.add(99);
        test2.add(999);
        test2.add(9999);
        test.add(test2);
        test.show();
        System.out.println("-----------");
        MyChain<Integer> test_constructor = new MyChain<>(1,6);
        test_constructor.show();
        System.out.println("-----------");
        Integer[] arrays = new Integer[5];
        arrays[0]=1;
        System.out.println();
        MyChain<Integer> test_constructor2 = new MyChain<>(arrays);
        test_constructor2.show();
        System.out.println("-----------");
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        MyChain<Integer> test_constructor3 = new MyChain<>(arrayList);
        test_constructor3.show();
        System.out.println("-----------");
        System.out.println(test_constructor3.index(2));
        System.out.println("-----------");
        System.out.println(test_constructor3.contain(3));
        System.out.println(test_constructor3.contain(4));
//        test_constructor3.clear();
        test_constructor3.delete(2);
        test_constructor3.show();
        test_constructor3.clear();
        System.out.println("清除");
        test_constructor3.show();
    }
}
