package DataStuctrue.Chain;

/**
 * Created by fangzzzjjj on 16/7/13.
 */
public class Demo {
    public static void main(String[] args) throws OutOfBoundary {
        MyChain<Integer> test = new MyChain();
        test.add(1);
        test.add(2);
        test.add(3);
        test.add(4);
        test.show();
        System.out.println("------");
        test.add(0,99);
        test.show();
        System.out.println("------");
        test.add(4,99);
        test.show();
        System.out.println("现有元素"+test.length()+"个");
        System.out.println("------");
//        test.add(6,100); 抛异常
//        test.delete(-1); 抛越界异常
//        test.delete(4); 抛越界异常
        test.delete(3);
        test.show();
        System.out.println("------");
        test.change(1, 999);
        test.show();
        System.out.println("------");
        test.change(0, 999);
        test.show();
        System.out.println("------");
        System.out.print("索引4对应的元素是");
        test.show(4);
        System.out.println("------");
        System.out.println("测试翻转");
        for(int i=0;i<test.length();i++){
            test.change(i,i);
        }
        test.show();
        System.out.println("-------------");
        test.reverse();
        test.show();
    }
}
